﻿namespace MessagingCenter.Views.Pages
{
    using Xamarin.Forms;

    public partial class MessagesPage : ContentPage
    {
        public MessagesPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(myEntry.Text))
            {
                MessagingCenter.Send(this, "AddEmptyMessage");
                return;
            }

            MessagingCenter.Send(this, "AddMessage", myEntry.Text);
        }
    }
}