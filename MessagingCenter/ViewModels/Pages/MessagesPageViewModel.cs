﻿namespace MessagingCenter.ViewModels.Pages
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using MessagingCenter.Views.Pages;
    using Xamarin.Forms;

    public class MessagesPageViewModel : BaseViewModel
    {
        ObservableCollection<string> messages;
        public ObservableCollection<string> Messages
        {
            get
            {
                return messages;
            }
            set
            {
                if (value != messages)
                {
                    messages = value;
                    OnPropertyChanged();
                }
            }
        }

        public ICommand UnSubscribeCommand { get; set; }

        public MessagesPageViewModel()
        {
            UnSubscribeCommand = new Command(UnSubscribe);
            Messages = new ObservableCollection<string>();

            MessagingCenter.Subscribe<MessagesPage>(this, "AddEmptyMessage", (sender) =>
            {
                Messages.Add("Empty");
            });

            MessagingCenter.Subscribe<MessagesPage, string>(this, "AddMessage", (sender, message) =>
            {
                Messages.Add(message);
            });
        }

        private void UnSubscribe(object obj)
        {
            MessagingCenter.Unsubscribe<MessagesPage>(this, "AddEmptyMessage");
            MessagingCenter.Unsubscribe<MessagesPage, string>(this, "AddMessage");
        }
    }
}